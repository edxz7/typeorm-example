"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Artist_1 = require("../entities/Artist");
var Album_1 = require("../entities/Album");
// C del CRUD
exports.createAlbumView = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var artists;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, typeorm_1.getRepository(Artist_1.Artist).find()];
            case 1:
                artists = _a.sent();
                console.log(artists);
                res.render('createAlbum', { artists: artists });
                return [2 /*return*/];
        }
    });
}); };
exports.createAlbum = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, name, genre, image, rating, artistId, newAlbum, result, artist;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, name = _a.name, genre = _a.genre, image = _a.image, rating = _a.rating, artistId = _a.artistId;
                newAlbum = image ?
                    typeorm_1.getRepository(Album_1.Album).create({ name: name, genre: genre, cover_image: image, rating: rating, artist: artistId })
                    : typeorm_1.getRepository(Album_1.Album).create({ name: name, genre: genre, rating: rating, artist: artistId });
                return [4 /*yield*/, typeorm_1.getRepository(Album_1.Album).save(newAlbum)];
            case 1:
                result = _b.sent();
                return [4 /*yield*/, typeorm_1.getRepository(Artist_1.Artist).findOne(artistId)];
            case 2:
                artist = _b.sent();
                artist.albums = [newAlbum];
                res.redirect("/");
                return [2 /*return*/];
        }
    });
}); };
//Show album detail
exports.albumDetail = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var albumId, album;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("params AH!!!!! ", req.params.albumId);
                albumId = req.params.albumId;
                return [4 /*yield*/, typeorm_1.getRepository(Album_1.Album).findOne(albumId)];
            case 1:
                album = _a.sent();
                res.render('albumDetail', { album: album });
                return [2 /*return*/];
        }
    });
}); };
