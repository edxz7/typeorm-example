"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Artist_1 = require("./Artist");
var Album = /** @class */ (function () {
    function Album() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn('uuid'),
        __metadata("design:type", Number)
    ], Album.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Album.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Album.prototype, "genre", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Album.prototype, "rating", void 0);
    __decorate([
        typeorm_1.Column({ default: 'https://vinylboutique.co.uk/wp-content/uploads/2017/05/default-release-cd.png' }),
        __metadata("design:type", String)
    ], Album.prototype, "cover_image", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Artist_1.Artist; }, function (artist) { return artist.albums; }, { onDelete: 'CASCADE' }),
        typeorm_1.JoinColumn({ name: 'artist_id' }) // se especifica obligatoriamente en relaciones OtO y solo se define en la FK
        ,
        __metadata("design:type", Artist_1.Artist)
    ], Album.prototype, "artist", void 0);
    Album = __decorate([
        typeorm_1.Entity()
    ], Album);
    return Album;
}());
exports.Album = Album;
