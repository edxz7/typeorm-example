"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
require("reflect-metadata");
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var typeorm_1 = require("typeorm");
var app = express_1.default();
typeorm_1.createConnection().then(function (connection) {
    console.log("connected to: ", connection.name);
});
// configuramos body-parser
app.use(body_parser_1.default.urlencoded({ extended: true }));
// configure static files
app.use(express_1.default.static("public"));
// configure view engine
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");
// rendereamos una pagina para ver que todo funciona
// app.get('/', (req: Request, res: Response) => {
//   res.render('index');
// })
// // Rutas de demostracion
// app.get('/results', (req: Request, res: Response) => {
//   const { artist, album, genre } = req.query;
//   console.log(artist, album, genre)
//   res.render('results', { artist, album, genre });
// })
// app.get('/login', (req: Request, res: Response) => {
//   res.render('login');
// })
// app.post('/login', (req: Request, res: Response) => {
//   const { user, password } = req.body;
//   console.log(user, password)
// });
// Middleware
// Creamos una ruta de prueba
// app.use(myFakeMiddleware)
// function myFakeMiddleware(req: any, res: Response, next: Function){
//   console.log("myFakeMiddleware was called!");
//   req.secretValue = "secreto!!";
//   next();
// }
// app.get('/test', myFakeMiddleware, (req: any, res: Response) => {
//   let mySecret = req.secretValue;
//   res.send(mySecret);
// });
var index_routes_1 = __importDefault(require("./routes/index.routes"));
var artist_routes_1 = __importDefault(require("./routes/artist.routes"));
var album_routes_1 = __importDefault(require("./routes/album.routes"));
app.use("/", index_routes_1.default);
app.use("/", artist_routes_1.default);
app.use("/", album_routes_1.default);
app.listen(process.env.PORT, function () { return console.log("\uD83C\uDFC3\u200D\u2640\uFE0F on port: " + process.env.PORT); });
