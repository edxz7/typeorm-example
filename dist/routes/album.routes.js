"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var album_controller_1 = require("../controllers/album.controller");
var router = express_1.Router();
router.get('/create-album', album_controller_1.createAlbumView)
    .post('/create-album', album_controller_1.createAlbum)
    .get('/:albumId', album_controller_1.albumDetail);
exports.default = router;
