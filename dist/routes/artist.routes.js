"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var artist_controller_1 = require("../controllers/artist.controller");
var router = express_1.Router();
router.get('/create-artist', artist_controller_1.createArtistsView)
    .post('/create-artist', artist_controller_1.createArtist)
    .get('/update-artist/:artistId', artist_controller_1.updateArtistView)
    .post('/update-artist/:artistId', artist_controller_1.updateArtist)
    .get('/delete-artist/:artistId', artist_controller_1.deleteArtist);
exports.default = router;
