"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var artist_controller_1 = require("../controllers/artist.controller");
var router = express_1.Router();
router.get('/', artist_controller_1.getArtists);
exports.default = router;
