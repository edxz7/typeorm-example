import { Router } from 'express';
import { createAlbumView, createAlbum, albumDetail } from '../controllers/album.controller';
const router = Router();
router.get('/create-album', createAlbumView)
      .post('/create-album', createAlbum)
      .get('/:albumId', albumDetail)
export default router;