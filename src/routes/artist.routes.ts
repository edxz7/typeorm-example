import { Router } from 'express';
import { createArtist, 
         createArtistsView, 
         updateArtistView, 
         updateArtist,
         deleteArtist
       } from '../controllers/artist.controller';
const router = Router();

router.get('/create-artist', createArtistsView)
      .post('/create-artist', createArtist)
      .get('/update-artist/:artistId', updateArtistView)
      .post('/update-artist/:artistId', updateArtist)
      .get('/delete-artist/:artistId', deleteArtist)

export default router;