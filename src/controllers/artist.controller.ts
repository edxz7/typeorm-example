import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Artist } from '../entities/Artist';

// C del CRUD
export const createArtistsView = (req: Request, res: Response) => {
  res.render("createArtist");
}
export const createArtist = async (req: Request, res: Response) => {
  const { name, genre } = req.body;
  const newArtist = await getRepository(Artist).create({ name, genre });
  console.log(newArtist)
  const results = await getRepository(Artist).save(newArtist);
  console.log(results);
  res.redirect("/");
}

// R del CRUD
export const getArtists = async (req: Request, res: Response) => {
  const artists = await getRepository(Artist).find({ relations: ["albums"]});
  console.log(artists);
  res.render('index', {artists});
}

// U en CRUD
export const updateArtistView = async (req: Request, res: Response) => {
  const artist = await getRepository(Artist).findOne(req.params.artistId);
  res.render('updateArtist', artist);
}

export const updateArtist = async(req: Request, res: Response) => {
  const { id, name, genre } = req.body;
  const updatedArtist = await getRepository(Artist).findOne(id);
  if(updatedArtist) {
    getRepository(Artist).merge(updatedArtist, { name, genre});
    const results = await getRepository(Artist).save(updatedArtist);
    console.log('success', results);
    res.redirect('/');
  }
  return res.status(404).json({msg: 'User not found'});
}

// la D en CRUD
export const deleteArtist = async (req:Request, res: Response) => {
  console.log(req.params);
  const { artistId } = req.params;
  await getRepository(Artist).delete(artistId);
  res.redirect("/");
}