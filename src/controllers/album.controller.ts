import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Artist } from '../entities/Artist';
import { Album } from '../entities/Album';

// C del CRUD
export const createAlbumView = async (req:Request, res: Response) => {
  const artists = await getRepository(Artist ).find(); 
  console.log(artists)
  res.render('createAlbum', { artists });
}
export const createAlbum = async (req: Request, res: Response) => {
  const { name, genre, image, rating, artistId } = req.body;
  const newAlbum = image ? 
  getRepository(Album).create({ name, genre, cover_image: image, rating, artist: artistId })
  :getRepository(Album).create({ name, genre, rating, artist: artistId });
  const result = await getRepository(Album).save(newAlbum);
  // ahora debemos asociar este album a su artista correspondiente
  const artist = await getRepository(Artist).findOne(artistId);
  artist.albums = [newAlbum];
  res.redirect("/");
}


//Show album detail
export const albumDetail = async (req: Request, res: Response) => {
  console.log("params AH!!!!! ", req.params.albumId)
  const { albumId } = req.params;
  const album = await getRepository(Album).findOne(albumId);
  res.render('albumDetail', { album })
}