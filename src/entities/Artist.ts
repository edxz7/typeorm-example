import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { Album } from './Album';
@Entity()
export class Artist {
  @PrimaryGeneratedColumn('uuid')
  id: number
  @Column()
  name: string
  @Column()
  genre: string
  @OneToMany(type => Album, album => album.artist)
  @JoinColumn({name: 'album_id'}) // es opcional en relaciones MtO (pero ayuda a renombrar fields)
  albums: Album[]
}