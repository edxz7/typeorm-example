import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Artist } from './Artist';

@Entity()
export class Album {
  @PrimaryGeneratedColumn('uuid')
  id: number
  @Column()
  name: string
  @Column()
  genre: string
  @Column()
  rating: string
  @Column({ default: 'https://vinylboutique.co.uk/wp-content/uploads/2017/05/default-release-cd.png' })
  cover_image: string
  @ManyToOne(type => Artist, artist => artist.albums, {onDelete: 'CASCADE'})
  @JoinColumn({ name: 'artist_id' }) // se especifica obligatoriamente en relaciones OtO y solo se define en la FK
  artist: Artist
}