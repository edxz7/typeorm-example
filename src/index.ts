require("dotenv").config();
import 'reflect-metadata';
import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import hbs from 'hbs';
import { createConnection } from 'typeorm';


const app = express();

createConnection().then(connection => {
  console.log("connected to: ", connection.name)
})

// configuramos body-parser
app.use(bodyParser.urlencoded({ extended: true }));
// configure static files
app.use(express.static("public"));
// configure view engine
app.set('view engine', 'hbs');
app.set('views', `${__dirname}/views`);
// rendereamos una pagina para ver que todo funciona
// app.get('/', (req: Request, res: Response) => {
//   res.render('index');
// })

// // Rutas de demostracion
// app.get('/results', (req: Request, res: Response) => {
//   const { artist, album, genre } = req.query;
//   console.log(artist, album, genre)
//   res.render('results', { artist, album, genre });
// })
// app.get('/login', (req: Request, res: Response) => {
//   res.render('login');
// })
// app.post('/login', (req: Request, res: Response) => {
//   const { user, password } = req.body;
//   console.log(user, password)
// });

// Middleware
// Creamos una ruta de prueba
// app.use(myFakeMiddleware)
// function myFakeMiddleware(req: any, res: Response, next: Function){
//   console.log("myFakeMiddleware was called!");
//   req.secretValue = "secreto!!";
//   next();
// }

// app.get('/test', myFakeMiddleware, (req: any, res: Response) => {
//   let mySecret = req.secretValue;
//   res.send(mySecret);
// });

import indexRoute from './routes/index.routes';
import artistRoutes from './routes/artist.routes';
import albumRoutes from './routes/album.routes';
app.use("/", indexRoute);
app.use("/", artistRoutes);
app.use("/", albumRoutes);

app.listen(process.env.PORT, () => console.log(`🏃‍♀️ on port: ${process.env.PORT}`) );